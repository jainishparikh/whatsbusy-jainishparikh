# WhatsBusy Django App :


### Steps To-do before starting application:
1. Update setting.py file in ./whatsbusy/whatsbusy/settings.py
- Insert database information for MYSQL (create a database and save database name, username and password for MYSQL)
- Insert your STRIPE_PUBLISHABLE_KEY and STRIPE_SECRET_KEY
2. Run commands:
- python manage.py makemigrations
- python manage.py migrate
- python manage.py runserver

3. Go to http://localhost:8000/
- default port is 8000
