class String_compression:
    def compress(self, s):
        # if string is empty return empty string
        if not s:
            return s

        # initialize a new empty string
        new_s, i, length_of_s, length_of_new_s = "", 0, len(s), 0

        # Iterate over the given string
        while i < length_of_s:
            start, count = s[i], 1

        # finding length of same consecutive characters
            while i < length_of_s-1 and s[i] == s[i+1]:
                count += 1
                i += 1
        # adding the character and its length to new string
            if count == 1:
                new_s += start
                length_of_new_s += 1
            else:
                new_s += (start+str(count))
                length_of_new_s += 2
            i += 1
        # if both strings are same, returning the original string
        if length_of_new_s >= length_of_s:
            return s
        return new_s


'''
time complexity - O(n)
-We are iteerating over the string only once. Although, there are nested while loops but same counter is incremented in both thus iterating over string only once.

space complexity - O(n)
- extra space for new string to be returned
'''
# test
if __name__ == "__main__":
    sc = String_compression()
    assert sc.compress('bbcceeee') == 'b2c2e4'
    assert sc.compress('aaabbbcccaaa') == 'a3b3c3a3'
    assert sc.compress('a') == 'a'
