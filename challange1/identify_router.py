class Graph:
    graph_dict = {}
    # add aedges to the graph

    def addEdge(self, node_1, node_2):
        if node_1 in self.graph_dict:
            self.graph_dict[node_1]["outbound"].append(node_2)
        else:
            self.graph_dict[node_1] = {"inbound": [], "outbound": [node_2]}

        if node_2 in self.graph_dict:
            self.graph_dict[node_2]["inbound"].append(node_1)
        else:
            self.graph_dict[node_2] = {"inbound": [node_1], "outbound": []}

    # return all graph in for of a dictionary
    def giveEdges(self):
        return self.graph_dict


def identify_router(graph):
    # get all edges
    g = graph.giveEdges()

    # initialize mx_connection to find node with maximum connections
    mx_connections = float('-inf')

    # initialize mx_node which will contain list of node/nodes with max connections
    mx_node = []

    # iterate over the graph to find node with max connections and update mx_node list
    for node in g:
        current_connections = len(
            g[node]["inbound"]) + len(g[node]["outbound"])
        if current_connections > mx_connections:
            mx_connections = current_connections
            mx_node = [node]
        elif current_connections == mx_connections:
            mx_node.append(node)

    # return list of node/nodes with maximum connections
    return mx_node


if __name__ == "__main__":
    # create a graph object
    graph = Graph()

    # gives edges to be added here
    edges_1 = [[1, 2], [2, 3], [3, 5], [5, 2], [2, 1]]
    edges_2 = [[1, 3], [3, 5], [5, 6], [6, 4], [4, 5], [5, 2], [2, 6]]
    edges_3 = [[2, 4], [4, 6], [6, 2], [2, 5], [5, 6]]

    # add edges to in the graph
    for edge in edges_3:
        graph.addEdge(edge[0], edge[1])
    # print the list of router or routers with maximum connections
    print(identify_router(graph))

